import argparse
import glob
import json
import numpy as np
import os
import natsort

from pose import Pose, Part, PoseSequence
from pprint import pprint


def main():

    parser = argparse.ArgumentParser(description='Pose Trainer Parser')
    parser.add_argument('--input_folder', type=str, default='poses', help='input folder for json files')
    parser.add_argument('--output_folder', type=str, default='poses_compressed', help='output folder for npy files')
    
    args = parser.parse_args()

    video_paths = glob.glob(os.path.join(args.input_folder, '*'))
    video_paths = sorted(video_paths)

    # Get all the json sequences for each video
    all_ps = []
    for video_path in video_paths:
        all_ps.append(parse_sequence(video_path, args.output_folder))
    return video_paths, all_ps

def sortedOrdering(all_keypoints, keypoints):
    # PART_NAMES = ['Nose', 'Neck',  'RShoulder', 'RElbow', 'RWrist', 'LShoulder', 'LElbow', 'LWrist', 'RHip', 'RKnee', 'RAnkle', 'LHip', 'LKnee', 'LAnkle', 'REye', 'LEye', 'REar', 'LEar']
    PART_NAMES = ['Nose', 'LEye', 'REye', 'LEar', 'REar', 'LShoulder', 'RShoulder', 'LElbow', 'RElbow', 'LWrist', 'RWrist', 'LHip', 'RHip', 'LKnee', 'RKnee', 'LAnkle', 'RAnkle']
    new_keyps = []
    previous = None
    if len(all_keypoints) > 0:
        previous = all_keypoints[-1]
    for part in PART_NAMES:
        if part in list(keypoints.keys()):
            indices = keypoints[part]
            # indices.append(1)
            new_keyps.append(indices)
        else:
            # new_keyps.append([-1,-1,0])
            if previous != None:
                val = previous[PART_NAMES.index(part)]
                new_keyps.append(val)
            else:
                new_keyps.append([0,0])
    return new_keyps

def select_max(a, b):
    if 'Neck' in a and 'Neck' in b:
        return(np.argmax([a['Neck'][0], b['Neck'][0]]))
    elif 'Nose' in a and 'Nose' in b:
        return(np.argmax([a['Nose'][0], b['Nose'][0]]))
    elif 'REar' in a and 'REar' in b:
        return(np.argmax([a['REar'][0], b['REar'][0]]))

    elif 'LEar' in a and 'LEar' in b:
        return(np.argmax([a['LEar'][0], b['LEar'][0]]))
    elif 'RShoulder' in a and 'RShoulder' in b:
        return(np.argmax([a['RShoulder'][0], b['RShoulder'][0]]))
    elif 'LShoulder' in a and 'LShoulder' in b:
        return(np.argmax([a['LShoulder'][0], b['LShoulder'][0]]))
    elif 'RWrist' in a and 'RWrist' in b:
        return(np.argmax([a['RWrist'][0], b['RWrist'][0]]))
    elif 'LWrist' in a and 'LWrist' in b:
        return(np.argmax([a['LWrist'][0], b['LWrist'][0]]))
    elif 'RKnee' in a and 'RKnee' in b:
        return(np.argmax([a['RKnee'][0], b['RKnee'][0]]))
    elif 'LKnee' in a and 'LKnee' in b:
        return(np.argmax([a['LKnee'][0], b['LKnee'][0]]))
    elif 'LEye' in a and 'LEye' in b:
        return(np.argmax([a['LEye'][0], b['LEye'][0]]))
    elif 'REye' in a and 'REye' in b:
        return(np.argmax([a['REye'][0], b['REye'][0]]))
    elif 'LEar' in a and 'LEar' in b:
        return(np.argmax([a['LEar'][0], b['LEar'][0]]))
    elif 'REar' in a and 'REar' in b:
        return(np.argmax([a['REar'][0], b['REar'][0]]))
    elif 'RHip' in a and 'RHip' in b:
        return(np.argmax([a['RHip'][0], b['RHip'][0]]))
    elif 'LHip' in a and 'LHip' in b:
        return(np.argmax([a['LHip'][0], b['LHip'][0]]))
    elif 'RAnkle' in a and 'RAnkle' in b:
        return(np.argmax([a['RAnkle'][0], b['RAnkle'][0]]))
    elif 'LAnkle' in a and 'LAnkle' in b:
        return(np.argmax([a['LAnkle'][0], b['LAnkle'][0]]))
        

def parse_sequence(json_folder, output_folder):
    """Parse a sequence of OpenPose JSON frames and saves a corresponding numpy file.

    Args:
        json_folder: path to the folder containing OpenPose JSON for one video.
        output_folder: path to save the numpy array files of keypoints.

    """
    json_files = glob.glob(os.path.join(json_folder, '*.json'))
    json_files = natsort.natsorted(json_files)

    num_frames = len(json_files)
    all_keypoints = []
    for i in range(num_frames):
        with open(json_files[i]) as f:
            json_obj = json.load(f)
            # if json_obj != []:
            if json_obj != [] and len(json_obj) == 1:
                keypoints = json_obj[0]
                keypoints = sortedOrdering(all_keypoints, keypoints)
                all_keypoints.append(keypoints)
            elif len(json_obj) >= 2:
                idx = select_max(json_obj[0], json_obj[1])
                if idx == None:
                    idx = 0
                keypoints = json_obj[idx]
                keypoints = sortedOrdering(all_keypoints, keypoints)
                all_keypoints.append(keypoints)
            
    
    output_dir = os.path.join(output_folder, os.path.basename(json_folder))
    np.save(output_dir, all_keypoints)


def load_ps(filename):
    """Load a PoseSequence object from a given numpy file.

    Args:
        filename: file name of the numpy file containing keypoints.
    
    Returns:
        PoseSequence object with normalized joint keypoints.
    """
    all_keypoints = np.load(filename)
    return PoseSequence(all_keypoints)


if __name__ == '__main__':
    main()

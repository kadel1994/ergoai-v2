import sys
import subprocess
import os


vid_name = sys.argv[1]
limit = -1
current_dir = os.path.dirname(os.path.realpath(__file__))
out_file = current_dir+'/final_outputs/final_output.mp4'

# remove existing videos
detectron_path = '../ergoai-v3/detectron/'
dir_to_remove = current_dir+'/'+detectron_path+'videos_input/'
items = os.listdir(dir_to_remove)

for item in items:
    if item.endswith('.mp4'):
        os.remove(os.path.join(dir_to_remove, item))

# copy input video
os.system('cp videos/'+vid_name + ' ../ergoai-v3/detectron/videos_input/'+vid_name )


os.chdir(detectron_path)
cmd_infer_video = "python tools/infer_video.py --cfg configs/12_2017_baselines/e2e_keypoint_rcnn_R-101-FPN_s1x.yaml --output-dir outputs --image-ext mp4 --wts https://dl.fbaipublicfiles.com/detectron/37698009/12_2017_baselines/e2e_keypoint_rcnn_R-101-FPN_s1x.yaml.08_45_57.YkrJgP6O/output/train/keypoints_coco_2014_train:keypoints_coco_2014_valminusminival/generalized_rcnn/model_final.pkl videos_input/"
os.system(cmd_infer_video)

os.chdir('../../ergoai-v2/data')
cmd_prepare_custom_data = "python prepare_data_2d_custom.py -i ../../ergoai-v3/detectron/outputs/ -o myvideos"
os.system(cmd_prepare_custom_data)

os.chdir('../')
cmd_pose_estimation = "python run.py -d custom -k myvideos -arc 3,3,3,3,3 -c checkpoint --evaluate pretrained_h36m_detectron_coco.bin --render --viz-subject " + vid_name + " --viz-action custom --viz-camera 0 --viz-limit " + str(limit) + " --viz-video videos/" + vid_name + " --viz-output " + out_file + " --viz-size 6 --viz-export ./3d-cordinates.npy"
os.system(cmd_pose_estimation)

# Opening report and video file

current_dir = os.path.dirname(os.path.realpath(__file__))
file = current_dir + '/final_outputs/' + vid_name[:-5] + '_report.pdf'
print("Path of report pdf file is ", file)
subprocess.call(('xdg-open',file))

file = out_file
print("Path of video file is ", file)
subprocess.call(('xdg-open',file))


import numpy as np
import sklearn.preprocessing as preprocessing
import math
import enum
from SkeletalAngles import JointIndices,angle,angle_from_horizontal,angle_from_vertical,radian_to_degree,joint_angle,get_body_height_in_pixels



BW = 190
L  = 30
H  = 72


"""
    ---------------------------------------------------------------------------------------------------------------------------
    Following is code to calculate the back compressive force model defined in the paper
      A REVISED BACK COMPRESSIVE FORCE ESTIMATION MODEL FOR ERGONOMIC EVALUATION OF LIFTING TASKS
      Manndi C. Loertscher, Andrew S. Merryweather, Donald S. Bloswick
    ---------------------------------------------------------------------------------------------------------------------------
"""
def back_compressive_force(data,frame_idx):
    """
      This function calculates the estimated back compressive force based on the original model

      In the following we use:

      BW    subject's weight in pounds
      L     weight of  the load
      HB    horizontal distance from the hands to the L5/S1 in inches
      Omega torso angle with horizontal
    """

    hip           = data[frame_idx,JointIndices.Hip_idx]
    thorax        = data[frame_idx,JointIndices.Thorax_idx]

    omega         = angle_from_vertical(thorax,hip)

    hands_mean    = (data[frame_idx,13] + data[frame_idx])/2
    hands_mean[2] = hip[2]            # projection of the averaged hand position to the horizontal plane crossing the hip
    hb            = np.linalg.norm(hands_mean)

    f             = 3 * BW + math.cos(omega) + (L * BW) / 2 + 0.8  * (BW / 2 + L)
    
    return f

    
def scale(data,frame_idx,H):
    return H / get_body_height_in_pixels(data,frame_idx)

def revised_back_compressive_force(data,frame_idx):
    """
      This function calculates the estimated back compressive force based on the model in the paper
      A REVISED BACK COMPRESSIVE FORCE ESTIMATION MODEL FOR ERGONOMIC EVALUATION OF LIFTING TASKS
      Manndi C. Loertscher, Andrew S. Merryweather, Donald S. Bloswick

      In the following we use:

      BW      subject's weight in pounds
      L       weight of  the load
      HB      horizontal distance from the hands to the L5/S1 in inches
      Omega   torso angle with horizontal

      The torso length (which is the hip to center of mass distance) can be approximated as 62% of 
      the hip to shoulder distance. Hip to shoulder distance is approximated as height * 0.28.
      
      The first term in the revised model .045(BW)(H) ~ 0.25 * .62 * .28 H. When setting torso
      length (= .62 * .28 H) to 12 inches, the previous model falls out. Since torso length is not
      obtained through height, the first term of the model becomes 0.25 * torso length
    """
    ##
    hip           = data[frame_idx,JointIndices.Hip_idx.value]
    thorax        = data[frame_idx,JointIndices.Thorax_idx.value]

    omega         = angle_from_horizontal(thorax,hip)

    hands_mean    = (data[frame_idx,JointIndices.Right_wrist_idx.value] + data[frame_idx,JointIndices.Left_wrist_idx.value])/2
    hands_mean[1] = hip[1]            # projection of the averaged hand position to the horizontal plane crossing the hip
    HB            = np.linalg.norm(hands_mean) * scale(data,frame_idx,H)
        
    f             = 0.045 * H * BW + math.cos(omega) + (L * HB) / 2 + 0.8  * (BW / 2 + L)
 
    return f




def evaluate_frame(data,frame_idx):
    f = revised_back_compressive_force(data,frame_idx)
    return (f,'No damaging force','yellow')


if __name__ == '__main__':
    data = np.load('3d-cordinates.npy')
    print(data.shape)
    print(joint_angle(data,'Right elbow',3))

    print(back_compressive_force(data,0))
    print(revised_back_compressive_force(data,0))

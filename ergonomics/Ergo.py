
import numpy as np
import RULA, BackCompressiveForceEstimation
import SkeletalAngles
from weasyprint import HTML
import datetime 
import os

def calculate_ergonomics_for_frame(data,frame_idx):
    e1  = RULA.evaluate_frame(data,frame_idx)
    e2 =  BackCompressiveForceEstimation.evaluate_frame(data,frame_idx)

    return { 'RULA'  : e1 ,
             'Force' : e2 }

def test_calculate_frame():
    data = np.load('3d-cordinates.npy')
    values1 = np.zeros(shape=(data.shape[0],1),dtype=float)
    values2 = np.zeros(shape=(data.shape[0],1),dtype=float)
    for i in range(data.shape[0]):
        e = calculate_ergonomics_for_frame(data,i) 
        print(e)
        values1[i] = e['RULA'][0]
        values2[i] = e['Force'][0]

    print("RULA  = " + str(np.mean(values1)) + ' ' + str(np.std(values1)) + ' ' + str(np.min(values1)) + ' ' + str(np.max(values1)))
    print("Force = " + str(np.mean(values2)) + ' ' + str(np.std(values2)) + ' ' + str(np.min(values2)) + ' ' + str(np.max(values2)))
    
def test_part_angles():
    data = np.load('3d-cordinates.npy')

    for i in range(data.shape[0]):
        for p in 'Left lower arm','Right lower arm','Left upper arm','Right upper arm':
            print(SkeletalAngles.radian_to_degree(SkeletalAngles.part_angle_from_vertical(data,i,p) ),' ',end='')
        print(SkeletalAngles.radian_to_degree(SkeletalAngles.joint_angle(data,i,'Left elbow') ),' ',end='')
        print('')

def createReport(rula_score,bcf_score,outputFile):
    now = datetime.datetime.now()
    folder = os.path.dirname(os.path.realpath(__file__))
    print("FOLDER = ",folder)
    with open(folder + '/report/index.html', 'r') as file: 
        report = file.read().replace('$date$', now.strftime("%Y-%m-%d %H:%M") ).replace('$risk$' ,str(rula_score) ).replace('$bcf$' ,str(bcf_score))
    print("Writing PDF file to '" + outputFile + "' ...",flush=True)
    HTML(string = report).write_pdf(outputFile)

def analyzeAndCreateReport(keypoints3DFileName,outputPDFFileName):
    data = np.load(keypoints3DFileName)
    values1 = np.zeros(shape=(data.shape[0],1),dtype=float)
    values2 = np.zeros(shape=(data.shape[0],1),dtype=float)
    for i in range(data.shape[0]):
        e = calculate_ergonomics_for_frame(data,i) 
        values1[i] = e['RULA'][0]
        values2[i] = e['Force'][0]

    result = [ [ round(np.mean(values1),2) , round(np.std(values1),2) , round(np.min(values1),2) , round(np.max(values1),2) ],
               [ round(np.mean(values2),2) , round(np.std(values2),2) , round(np.min(values2),2) , round(np.max(values2),2) ] ]

    print(result)
    
    createReport(result[0][0],result[1][0],outputPDFFileName)

if __name__ == '__main__':
    analyzeAndCreateReport('3d-cordinates.npy','report.pdf')
    #test_part_angles()
    #test_calculate_frame()

    

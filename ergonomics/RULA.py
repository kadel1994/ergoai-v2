
import sys
import numpy as np
from SkeletalAngles import JointIndices,angle,angle_from_vertical,radian_to_degree,joint_angle,part_angle_from_vertical,part_angle_from_horizontal

import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


"""
    ---------------------------------------------------------------------------------------------------------------------------
    RULA table definitions 
    ---------------------------------------------------------------------------------------------------------------------------
"""
table_a = np.array(
          [[[[1,	2],	[2,	2],	[2,	3],	[3,	3]],
            [[2,	2],	[2,	2],	[3,	3],	[3,	3]],
            [[2,	3],	[3,	3],	[3,	3],	[4,	4]]],
           [[[2,	3],	[3,	3],	[3,	4],	[4,	4]],
            [[3,	3],	[3,	3],	[3,	4],	[4,	4]],
            [[3,	4],	[4,	4],	[4,	4],	[5,	5]]],
           [[[3,	3],	[4,	4],	[4,	4],	[5,	5]],
            [[3,	4],	[4,	4],	[4,	4],	[5,	5]],
            [[4,	4],	[4,	4],	[4,	5],	[5,	5]]],
           [[[4,	4],	[4,	4],	[4,	5],	[5,	5]],
            [[4,	4],	[4,	4],	[4,	5],	[5,	5]],
            [[4,	4],	[4,	5],	[5,	5],	[6,	6]]],
           [[[5,	5],	[5,	5],	[5,	6],	[6,	7]],
            [[5,	6],	[6,	6],	[6,	6],	[7,	7]],
            [[6,	6],	[6,	7],	[7,	7],	[7,	8]]],
           [[[7,	7],	[7,	7],	[7,	8],	[8,	9]],
            [[8,	8],	[8,	8],	[8,	9],	[9,	9]],
            [[9,	9],	[9,	9],	[9,	9],	[9,	9]]]],np.int32)

table_b = np.array(
           [[[1, 3], [2, 3], [3, 4], [5, 5], [6, 6], [7, 7]],
            [[2, 3], [2, 3], [4, 5], [5, 5], [6, 7], [7, 7]],
            [[3, 3], [3, 4], [4, 5], [5, 6], [6, 7], [7, 7]],
            [[5, 5], [5, 6], [6, 7], [7, 7], [7, 7], [8, 8]],
            [[7, 7], [7, 7], [7, 8], [8, 8], [8, 8], [8, 8]],
            [[8, 8], [8, 8], [8, 8], [8, 9], [9, 9], [9, 9]]],np.int32)

table_c = np.array(
            [[1, 2, 3, 3, 4, 5, 5],
             [2, 2, 3, 4, 4, 5, 5],
             [3, 3, 3, 4, 4, 5, 6],
             [3, 3, 3, 4, 5, 6, 6],
             [4, 4, 4, 5, 6, 7, 7],
             [4, 4, 5, 6, 6, 7, 7],
             [5, 5, 6, 6, 7, 7, 7],
             [5, 5, 6, 7, 7, 7, 7] ],np.int32)



"""
    ---------------------------------------------------------------------------------------------------------------------------
    Following is the code to calculate RULA
    ---------------------------------------------------------------------------------------------------------------------------
"""

class RULA:

    def __init__(self,verbose = False):
        self.verbose = verbose

    def eval_debug(self,data,frame_idx):
        self.__step_10_locate_trunk_position(data,frame_idx)
 

    def eval(self,data,frame_idx):
        row   = self.__steps_1_8(data,frame_idx) 
        col   = self.__steps_9_15(data,frame_idx)
        final = table_c[row-1,col-1]

        if final <= 2:
            text    = 'Acceptable'
            color   = 'green'
        elif final <= 4:
            text    = 'Investigate further'
            color   = 'yellow'
        elif final <= 6:
            text    = 'Investigate further and change soon'
            color   = 'orange'
        else:
            text    = 'Investigate and change immediately'
            color   = 'red'

        if self.verbose:
            print("Row (steps 1-8) = " + str(row) + ", col (steps 9-15) = " + str(col) + ": Final = " + str(final))
            print(text,color)

        return (final,text,color)


    def __steps_1_8(self,data,frame_idx):
        step1 = self.__step_1_locate_upper_arm_position(data,frame_idx)
        step2 = self.__step_2_locate_lower_arm_position(data,frame_idx)
        step3 = 1
        step4 = 1

        assert 1 <= step1 and step1 <= 6
        assert 1 <= step2 and step2 <= 3
        assert 1 <= step3 and step3 <= 4
        assert 1 <= step4 and step4 <= 2

        step5 = table_a[step1-1,step2-1,step3-1,step4-1]
        if self.verbose:
            print("Step 1 = " + str(step1))
            print("Step 2 = " + str(step2))
            print("Step 3 = " + str(step3))
            print("Step 4 = " + str(step4))
            print("Step 5 = " + str(step5))
        step6 = 0
        step7 = 0

        step8 = step5 + step6 + step7

        if step8 > 8:
            step8 = 8

        return step8


    def __steps_9_15(self,data,frame_idx):
        step9   = self.__step_9_locate_neck_position(data,frame_idx)
        step10  = self.__step_10_locate_trunk_position(data,frame_idx)
        step11  = 1

        assert 1 <= step9  and step9  <= 6
        assert 1 <= step10 and step10 <= 6
        assert 1 <= step11 and step11 <= 2

        step12 = table_b[step9 - 1, step10 - 1, step11 - 1]
        step13 = 0
        step14 = 0

        step15 = step12 + step13 + step14
        if step15 > 7:
            step15 = 7

        if self.verbose:
            print("Step  9 = " + str(step9))
            print("Step 10 = " + str(step10))
            print("Step 11 = " + str(step11))
            print("Step 12 = " + str(step12))
            print("Step 15 = " + str(step15))

        return step15


    def __step_1_locate_upper_arm_position(self,data,frame_idx):
        table   = ( (-360, -90, 4),
                    ( -90, -45, 3),
                    ( -45, -20, 2),
                    ( -20,  20, 1),
                    (  20,  45, 2),
                    (  45,  90, 3),
                    (  90, 360, 4) )
                    
        left    = self.__locate_position(data,frame_idx,table,JointIndices.Left_shoulder_idx.value,
                                          JointIndices.Left_elbow_idx.value,'Left upper arm position')
        right   = self.__locate_position(data,frame_idx,table,JointIndices.Right_shoulder_idx.value,
                                          JointIndices.Right_elbow_idx.value,'Right upper arm position')
        
        return int((left + right) / 2)


    def __step_2_locate_lower_arm_position(self,data,frame_idx):
        """
          
        """
        table   = ( (-360,  0,2), # missing from worksheet
                    (   0, 60,2),
                    (  60,100,1),
                    ( 100,360,2) )
        left    = self.__locate_position(data,frame_idx,table,JointIndices.Left_elbow_idx.value,
                                          JointIndices.Left_wrist_idx.value,'Left lower arm position')
        right   = self.__locate_position(data,frame_idx,table,JointIndices.Right_elbow_idx.value,
                                          JointIndices.Right_wrist_idx.value,'Right lower arm position')
        
        return int((left + right) / 2)
        

    def __step_9_locate_neck_position(self,data,frame_idx):
        table   = ( (-360,  0,4),
                    (   0, 10,1),
                    (  10, 20,2),
                    (  20,360,3) )   
        a       = self.__locate_position(data,frame_idx,table,JointIndices.Head_idx.value,
                                                    JointIndices.Thorax_idx.value,'Neck position')

        return a


    def __step_10_locate_trunk_position(self,data,frame_idx):
        table   = ( (-360,-10,2),
                    ( -10,  0,1),
                    (   0, 20,2),
                    (  20, 60,3),
                    (  60,360,4) )
        a       = self.__locate_position(data,frame_idx,table,JointIndices.Thorax_idx.value,JointIndices.Hip_idx.value,'Trunk Position')
        return a


    def __locate_position(self,data,frame_idx,table,joint_idx1,joint_idx2,title,is_vertical = True):
        """
          calculates the angle between the line with endpoints of joint1 and 2 and the vertical,
          then maps the resulting value to an integer based on the given interval table
        """
        joint1 = data[frame_idx,joint_idx1]
        joint2 = data[frame_idx,joint_idx2]

        if is_vertical:
            angle  = radian_to_degree(angle_from_vertical(joint1,joint2) )
        else:
            angle  = radian_to_degree(angle_from_horizontal(joint1,joint2) )

        if self.verbose:
            print(title)
            print(joint1,joint2)
            print(joint2-joint1)
            if is_vertical:
                print('Angle (vertical)   = ' + str(angle))
            else:
                print('Angle (horizontal) = ' + str(angle))

        if not table is None: 
            for interval in table:
                if interval[0] <= angle and angle < interval[1]:
                    if self.verbose:
                        print('Value = ' + str(interval[2]))
                        print('')
                    return interval[2]
            print('Value not found')
            print(table)
            if is_vertical:
                print('Angle (vertical)   = ' + str(angle))
            else:
                print('Angle (horizontal) = ' + str(angle))
            raise ValueError
        



def evaluate_frame(data,frame_idx,verbose = False):
    r = RULA(verbose)
    return r.eval(data,frame_idx)

"""
    Hip_idx             = 0
    Right_hip_idx       = 1
    Right_knee_idx      = 2
    Right_foot_idx      = 3
    Left_hip_idx        = 4
    Left_knee_idx       = 5
    Left_foot_idx       = 6
    Spine_idx           = 7
    Thorax_idx          = 8
    Nose_idx            = 9
    Head_idx            = 10
    Left_shoulder_idx   = 11
    Left_elbow_idx      = 12
    Left_wrist_idx      = 13
    Right_shoulder_idx  = 14
    Right_elbow_idx     = 15
    Right_wrist_idx     = 16
"""

color_Left    = 'red'
color_Right   = 'green'
color_Center  = 'black'

lines = [ (JointIndices.Hip_idx.value,        JointIndices.Left_hip_idx.value,    color_Left),  # left leg
          (JointIndices.Left_hip_idx.value,   JointIndices.Left_knee_idx.value,   color_Left),
          (JointIndices.Left_knee_idx.value,  JointIndices.Left_foot_idx.value,   color_Left),

          (JointIndices.Hip_idx.value,        JointIndices.Right_hip_idx.value,   color_Right),  # right leg
          (JointIndices.Right_hip_idx.value,  JointIndices.Right_knee_idx.value,  color_Right),
          (JointIndices.Right_knee_idx.value, JointIndices.Right_foot_idx.value,  color_Right),

         # (JointIndices.Hip_idx.value,        JointIndices.Spine_idx.value,       color_Center),
         # (JointIndices.Spine_idx.value,      JointIndices.Thorax_idx.value,      color_Center),
          (JointIndices.Thorax_idx.value,     JointIndices.Nose_idx.value,        color_Center),
          (JointIndices.Nose_idx.value,       JointIndices.Head_idx.value,        color_Center),

          (JointIndices.Thorax_idx.value,         JointIndices.Right_shoulder_idx.value,  color_Right),
          (JointIndices.Right_shoulder_idx.value, JointIndices.Right_elbow_idx.value,     color_Right),
          (JointIndices.Right_elbow_idx.value,    JointIndices.Right_wrist_idx.value,     color_Right),

          (JointIndices.Thorax_idx.value,         JointIndices.Left_shoulder_idx.value,  color_Left),
          (JointIndices.Left_shoulder_idx.value, JointIndices.Left_elbow_idx.value,     color_Left),
          (JointIndices.Left_elbow_idx.value,    JointIndices.Left_wrist_idx.value,     color_Left)

]

def plot(data,frame_idx):
    mpl.rcParams['legend.fontsize'] = 10

    fig   = plt.figure()
    ax    = fig.gca(projection='3d')
    x,y,z = 0,1,2

    for line in lines:
        p1 = data[frame_idx,line[0]]
        p2 = data[frame_idx,line[1]]
        c  = line[2]

        ax.plot([p1[x],p2[x]], [p1[y],p2[y]],[p1[z],p2[z]],c)

    for i in range(17):
        p = data[frame_idx,i]
        
        ax.scatter(p[x], p[y], p[z], marker='o',color='blue')
    ax.legend()
    ax.set_xlabel('X axis')
    ax.set_ylabel('Y axis')
    ax.set_zlabel('Z axis')
    print(ax.azim)
    plt.savefig('/mnt/c/tmp/test.png')

if __name__ == '__main__':
    data = np.load('3d-cordinates.npy')
    idx  = 18
    print(data[idx])
    print(radian_to_degree(joint_angle(data,idx,'Left knee')))
    print(radian_to_degree(joint_angle(data,idx,'Right knee')))
    print(radian_to_degree(joint_angle(data,idx,'Left elbow')))
    print(radian_to_degree(joint_angle(data,idx,'Right elbow')))

    a = data[idx,JointIndices.Left_elbow_idx.value]
    b = data[idx,JointIndices.Left_wrist_idx.value]
    print(a)
    print(b)
    print(b-a)
    print(radian_to_degree(part_angle_from_vertical(data,idx,'Left lower arm')))
    print(radian_to_degree(part_angle_from_horizontal(data,idx,'Left lower arm')))

    print('')
    print(radian_to_degree(angle_from_vertical(np.array([0,0,0]),np.array([0,0,1]))))
    print(radian_to_degree(angle_from_vertical(np.array([0,0,0]),np.array([0,1,0]))))
    print(radian_to_degree(angle_from_vertical(np.array([0,0,0]),np.array([1,0,0]))))
    print(radian_to_degree(part_angle_from_vertical(data,0,'Left lower arm')))
    plot(data,idx)

    print('-- debug starts --')
    r = RULA(verbose = False)
    for idx in range(data.shape[0]):
        r.eval(data,idx)
    print('-- debug ends   --')
    #print(radian_to_degree(joint_angle(data,0,'Left elbow')))
    #evaluate_frame(data,1,True)
    #r = RULA()
    #for i in range(data.shape[0]):
    #  print(r.eval(data,i) )

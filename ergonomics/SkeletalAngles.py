
import numpy as np
import sklearn.preprocessing as preprocessing
import math
import enum


"""
    ---------------------------------------------------------------------------------------------------------------------------
    FaceBook's Pose 3d indices  
    ---------------------------------------------------------------------------------------------------------------------------
"""
joint_names = ['Hip (root)', 'Right hip', 'Right knee', 'Right foot', 'Left hip', 'Left knee', 
               'Left foot', 'Spine', 'Thorax', 'Nose', 'Head', 'Left shoulder', 'Left elbow', 
               'Left wrist', 'Right shoulder', 'Right elbow', 'Right wrist']

class JointIndices(enum.IntEnum):               
    Hip_idx             = 0
    Right_hip_idx       = 1
    Right_knee_idx      = 2
    Right_foot_idx      = 3
    Left_hip_idx        = 4
    Left_knee_idx       = 5
    Left_foot_idx       = 6
    Spine_idx           = 7
    Thorax_idx          = 8
    Nose_idx            = 9
    Head_idx            = 10
    Left_shoulder_idx   = 11
    Left_elbow_idx      = 12
    Left_wrist_idx      = 13
    Right_shoulder_idx  = 14
    Right_elbow_idx     = 15
    Right_wrist_idx     = 16



def __get_joint_index(joint_name):
    return joint_names.index(joint_name)


def __cos_angle(a,b,c):
    v1    = a - b
    v2    = c - b
    v1    = v1 / np.linalg.norm(v1)
    v2    = v2 / np.linalg.norm(v2)
    dot   = np.dot(v1,v2)
    if dot > 1:
        dot = 1
    elif dot < -1:
        dot = -1

    return dot


def angle(a,b,c):
    return math.acos(__cos_angle(a,b,c))


def angle_from_vertical(a,b):
    """
      This function returns the angle at point B between the line (AB) and the vertical line going through point B
    """
    c     = np.array([b[0],b[1]+1,b[2]])
    d     = angle(a,b,c)

    if d > math.pi / 2.0:
        d = math.pi - d

    return d


def angle_from_horizontal(a,b):
    """
      This function returns the angle at point B between the line (AB) and the horizontal plane going through point B
    """
    return math.pi / 2 -  angle_from_vertical(a,b)


def radian_to_degree(rad):
    return rad * 180 / math.pi


def joint_angle(data,frame_idx,joint_name):
    """
        This function calculates the angle for the given joint. It retrieves the 2 neighboring points
        to define two lines (A,B) and (B,C). It assumes that the joints in questions A B C are next
        to each other in the joint_names array. See
        https://stackoverflow.com/questions/19729831/angle-between-3-points-in-3d-space
        for the math
    """

    b_idx = __get_joint_index(joint_name)
    a_idx = b_idx - 1
    c_idx = b_idx + 1

    return angle(data[frame_idx,a_idx],data[frame_idx,b_idx],data[frame_idx,c_idx])


def part_angle_from_vertical(data,frame_idx,body_part):
    if body_part == 'Left lower arm':
        idx1 = JointIndices.Left_elbow_idx.value
        idx2 = JointIndices.Left_wrist_idx.value
    elif body_part == 'Right lower arm':
        idx1 = JointIndices.Right_elbow_idx.value
        idx2 = JointIndices.Right_wrist_idx.value
    elif body_part == 'Left upper arm':
        idx1 = JointIndices.Left_shoulder_idx.value
        idx2 = JointIndices.Left_elbow_idx.value
    elif body_part == 'Right upper arm':
        idx1 = JointIndices.Right_shoulder_idx.value
        idx2 = JointIndices.Right_elbow_idx.value
    else:
        raise ValueError

    return angle_from_vertical(data[frame_idx,idx1],data[frame_idx,idx2])


def part_angle_from_horizontal(data,frame_idx,body_part):
    return math.pi / 2 - part_angle_from_vertical(data,frame_idx,body_part)


def get_body_height_in_pixels(data,frame_idx):
    return 6


if __name__ == '__main__':
    data = np.load('3d-cordinates.npy')
    print(data.shape)
    print(joint_angle(data,'Right elbow',3))

    print(revised_back_compressive_force(data,0))
    for joint_name in joint_names:
      print(joint_name.replace(' ','_') + '_idx =',__get_joint_index(joint_name))

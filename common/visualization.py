# Copyright (c) 2018-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.
#

import matplotlib
matplotlib.use('Agg')
from matplotlib.lines import Line2D      

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation, writers
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import cv2
import subprocess as sp
from object_recognition.yolo_utils import show_image, infer_image
import sys
sys.path.append('ergonomics')
import SkeletalAngles
import matplotlib.patches as patches
from Ergo import calculate_ergonomics_for_frame
from matplotlib import gridspec


def get_resolution(filename):
    command = ['ffprobe', '-v', 'error', '-select_streams', 'v:0',
               '-show_entries', 'stream=width,height', '-of', 'csv=p=0', filename]
    with sp.Popen(command, stdout=sp.PIPE, bufsize=-1) as pipe:
        for line in pipe.stdout:
            w, h = line.decode().strip().split(',')
            return int(w), int(h)
            
def get_fps(filename):
    command = ['ffprobe', '-v', 'error', '-select_streams', 'v:0',
               '-show_entries', 'stream=r_frame_rate', '-of', 'csv=p=0', filename]
    with sp.Popen(command, stdout=sp.PIPE, bufsize=-1) as pipe:
        for line in pipe.stdout:
            a, b = line.decode().strip().split('/')
            return int(a) / int(b)

def read_video(filename, skip=0, limit=-1):
    w, h = get_resolution(filename)
    
    command = ['ffmpeg',
            '-i', filename,
            '-f', 'image2pipe',
            '-pix_fmt', 'rgb24',
            '-vsync', '0',
            '-vcodec', 'rawvideo', '-']
    
    i = 0
    with sp.Popen(command, stdout = sp.PIPE, bufsize=-1) as pipe:
        while True:
            data = pipe.stdout.read(w*h*3)
            if not data:
                break
            i += 1
            if i > limit and limit != -1:
                continue
            if i > skip:
                yield np.frombuffer(data, dtype='uint8').reshape((h, w, 3))
            
                
                
    
def downsample_tensor(X, factor):
    length = X.shape[0]//factor * factor
    return np.mean(X[:length].reshape(-1, factor, *X.shape[1:]), axis=1)

def render_animation(keypoints, keypoints_metadata, poses, skeleton, fps, bitrate, azim, output, viewport,
                     limit=-1, downsample=1, size=6, input_video_path=None, input_video_skip=0, hyperparameters=None):
    """
    TODO
    Render an animation. The supported output modes are:
     -- 'interactive': display an interactive figure
                       (also works on notebooks if associated with %matplotlib inline)
     -- 'html': render the animation as HTML5 video. Can be displayed in a notebook using HTML(...).
     -- 'filename.mp4': render and export the animation as an h264 video (requires ffmpeg).
     -- 'filename.gif': render and export the animation a gif file (requires imagemagick).
    """
    plt.ioff()

    # adding gridspec module for unequal subplot sizes
    gs = gridspec.GridSpec(1, len(poses)+1, width_ratios=[4] + [1]*(len(poses))) # example for width_ratios for len(poses)=3 --> width_ratios = [3,2,2]
    

    fig = plt.figure(figsize=(size*(1 + len(poses)), size))
    # ax_in = fig.add_subplot(1, 1 + len(poses), 1)
    ax_in = fig.add_subplot(gs[0])
    ax_in.get_xaxis().set_visible(False)
    ax_in.get_yaxis().set_visible(False)
    ax_in.set_axis_off()
    ax_in.set_title('Input')
    print("\n\n\n===================================================================================================================")
    print("keypoints\n", keypoints.shape)
    print("poses\n", poses['Reconstruction'].shape) #(1621,17,3)
    ax_3d = []
    lines_3d = []
    trajectories = []
    radius = 1.7
    for index, (title, data) in enumerate(poses.items()):
        # ax = fig.add_subplot(1, 1 + len(poses), index+2, projection='3d')
        ax = fig.add_subplot(gs[index+1], projection='3d')
        ax.view_init(elev=15., azim=azim)
        ax.set_xlim3d([-radius/2, radius/2])
        ax.set_zlim3d([0, radius])
        ax.set_ylim3d([-radius/2, radius/2])
        ax.set_aspect('equal')
        # ax.set_xticklabels([])
        # ax.set_yticklabels([])
        # ax.set_zticklabels([])
        ax.dist = 7.5
        ax.set_title(title) #, pad=35
        ax.set_xlabel('X axis')
        ax.set_ylabel('Y axis')
        ax.set_zlabel('Z axis')
        ax_3d.append(ax)
        lines_3d.append([])
        trajectories.append(data[:, 0, [0, 1]]) #storing hip (x,y) values for every frame -> shape = (num_frames, 2)
    poses = list(poses.values())

    # Decode video
    if input_video_path is None:
        # Black background
        all_frames = np.zeros((keypoints.shape[0], viewport[1], viewport[0]), dtype='uint8')
    else:
        # Load video using ffmpeg
        all_frames = []
        for f in read_video(input_video_path, skip=input_video_skip, limit=limit):
            all_frames.append(f)
        
        effective_length = min(keypoints.shape[0], len(all_frames))
        all_frames = all_frames[:effective_length]
        
        keypoints = keypoints[input_video_skip:] # todo remove
        for idx in range(len(poses)):
            poses[idx] = poses[idx][input_video_skip:]
        
        # if fps is None:
        #     fps = get_fps(input_video_path)
        # print("frames per second ====>", fps)
        fps = 29.5
    
    if downsample > 1:
        keypoints = downsample_tensor(keypoints, downsample)
        all_frames = downsample_tensor(np.array(all_frames), downsample).astype('uint8')
        for idx in range(len(poses)):
            poses[idx] = downsample_tensor(poses[idx], downsample)
            trajectories[idx] = downsample_tensor(trajectories[idx], downsample)
        fps /= downsample

    print("-----------------------------------downsample-----------------------------------")
    print(keypoints.shape) #(631, 17, 2)
    print(poses[0].shape) #(631, 17, 3)
    print(len(all_frames)) #180
    print(all_frames[0].shape) #(336, 596, 3)

    initialized = False
    image = None
    lines = []
    points = None
    texts = [None]*16
    text_rula = None
    line_rula = None
    text_force = None
    line_force = None
    spline_line_1 = None
    spline_line_2 = None

    if limit < 1:
        limit = len(all_frames)
    else:
        limit = min(limit, len(all_frames))

    parents = skeleton.parents()
    print("--------------------------------------------parents--------------------------------------------")
    print(parents)
    print("1111111111111111111111")
    def update_video(i):
        
        nonlocal initialized, image, lines, points, texts, spline_line_1, spline_line_2,text_rula,text_force,line_rula,line_force

        for n, ax in enumerate(ax_3d):
            ax.set_xlim3d([-radius/2 + trajectories[n][i, 0], radius/2 + trajectories[n][i, 0]])
            ax.set_ylim3d([-radius/2 + trajectories[n][i, 1], radius/2 + trajectories[n][i, 1]])
        
        f = all_frames[i]
        object_detection_frame, foundPerson = get_frame(f, hyperparameters)

        if foundPerson == True:
            angles_dict = calculateAngles(keypoints[i], poses[0], i)
            ergo_dict   = calculate_ergonomics_for_frame(poses[0], i)

        # Update 2D poses
        joints_right_2d = keypoints_metadata['keypoints_symmetry'][1]
        colors_2d = np.full(keypoints.shape[1], 'black')
        colors_2d[joints_right_2d] = 'red'
            
        if not initialized:
            # image = ax_in.imshow(all_frames[i], aspect='equal')
            image = ax_in.imshow(object_detection_frame, aspect='equal')

            for j, j_parent in enumerate(parents):
                if j_parent == -1:
                    continue
                    
                if len(parents) == keypoints.shape[1] and keypoints_metadata['layout_name'] != 'coco':
                    # Draw skeleton only if keypoints match (otherwise we don't have the parents definition)
                    lines.append(ax_in.plot([keypoints[i, j, 0], keypoints[i, j_parent, 0]],
                                            [keypoints[i, j, 1], keypoints[i, j_parent, 1]], color='pink'))

                col = 'red' if j in skeleton.joints_right() else 'black'
                for n, ax in enumerate(ax_3d):
                    pos = poses[n][i]
                    lines_3d[n].append(ax.plot([pos[j, 0], pos[j_parent, 0]],
                                               [pos[j, 1], pos[j_parent, 1]],
                                               [pos[j, 2], pos[j_parent, 2]], zdir='z', c=col))
                

            points = ax_in.scatter(*keypoints[i].T, 10, color=colors_2d, edgecolors='white', zorder=10)

            # Legend
            bbox = image.get_window_extent()
            x,y     = 10,10
            factor  = 1
            width   = 200 * factor #(bbox.x1 - bbox.x0)/3
            height  =  60 * factor #(bbox.y1 - bbox.y0)/3
            print(width,height)
            #sys.exit(1)
            rect    = patches.Rectangle((x,y),width,height,edgecolor='white',facecolor='white', color='white')
            ax_in.add_patch(rect)

            text_rula   =  ax_in.annotate('RULA', (x + 10,y + 17 * factor),color='black', fontsize='small')
            text_force  =  ax_in.annotate('Force',(x + 10,y + 40 * factor),color='black', fontsize='small')
            line_rula   =  patches.Rectangle((x+10,y + 17 * factor + 5),width-20,5,color='red')
            line_force  =  patches.Rectangle((x+10,y + 40 * factor + 5),width-20,5,color='red')
            ax_in.add_patch(line_rula)
            ax_in.add_patch(line_force)
            # add angles
            for loc, (_, val) in enumerate(angles_dict.items()):
                keys = list(val.keys())
                pos_idx = 0 if keys[1] == 'color' else 1
                pos = keys[pos_idx]
                txt = val[pos]
                texts[loc*2] = ax_in.annotate(txt, pos, color=val['color'], fontsize='medium', fontstyle='italic', fontweight='medium')
                texts[loc*2+1] = ax_in.annotate(txt, pos, color='white', fontsize='medium', fontstyle='italic', fontweight='light')

            # # add spline line
            spline_x, spline_y = calculateSpline(keypoints[i])
            spline_line_1 = ax_in.add_line(Line2D(spline_x, spline_y, linewidth=3, color='white'))
            spline_line_2 = ax_in.add_line(Line2D(spline_x, spline_y, linewidth=3, color='black'))

            initialized = True
        else:
            # image.set_data(all_frames[i])
            image.set_data(object_detection_frame)

            for j, j_parent in enumerate(parents):
                if j_parent == -1:
                    continue
                
                if len(parents) == keypoints.shape[1] and keypoints_metadata['layout_name'] != 'coco':
                    lines[j-1][0].set_data([keypoints[i, j, 0], keypoints[i, j_parent, 0]],
                                           [keypoints[i, j, 1], keypoints[i, j_parent, 1]])

                for n, ax in enumerate(ax_3d):
                    pos = poses[n][i]
                    lines_3d[n][j-1][0].set_xdata([pos[j, 0], pos[j_parent, 0]])
                    lines_3d[n][j-1][0].set_ydata([pos[j, 1], pos[j_parent, 1]])
                    lines_3d[n][j-1][0].set_3d_properties([pos[j, 2], pos[j_parent, 2]], zdir='z')


            points.set_offsets(keypoints[i])
            
            # add angles
            for loc, (_, val) in enumerate(angles_dict.items()):
                keys = list(val.keys())
                pos_idx = 0 if keys[1] == 'color' else 1
                pos = keys[pos_idx]
                txt = val[pos]
                texts[loc*2].set_position(pos)
                texts[loc*2].set_text(txt)
                texts[loc*2].set_color(val['color'])
                texts[loc*2+1].set_position(pos)
                texts[loc*2+1].set_text(txt)

            # update legend
            text_rula.set_text(ergo_dict['RULA'][1])
            line_rula.set_color(ergo_dict['RULA'][2])
            text_force.set_text = (ergo_dict['Force'][1])
            line_force.set_color(ergo_dict['Force'][2])
            
            # # add spline line
            spline_x, spline_y = calculateSpline(keypoints[i])
            spline_line_1.set_xdata(spline_x)
            spline_line_1.set_ydata(spline_y)
            spline_line_2.set_xdata(spline_x)
            spline_line_2.set_ydata(spline_y)
        
        print('{}/{}      '.format(i, limit), end='\r')
        
    fig.tight_layout()
    anim = FuncAnimation(fig, update_video, frames=np.arange(0, limit), interval=1000/fps, repeat=False)
    print(output, flush=True)
    if output.endswith('.mp4'):
        Writer = writers['ffmpeg']
        writer = Writer(fps=fps, metadata={}, bitrate=bitrate)
        anim.save(output, writer=writer)
    elif output.endswith('.gif'):
        anim.save(output, dpi=80, writer='imagemagick')
    else:
        raise ValueError('Unsupported output format (only .mp4 and .gif are supported)')
    plt.close()

def get_frame(frame, hyperparameters):
    labels = open('./object_recognition/yolov3-coco/coco-labels').read().strip().split('\n')
    colors = np.random.randint(0, 255, size=(len(labels), 3), dtype='uint8')
    net = cv2.dnn.readNetFromDarknet(hyperparameters['config'], hyperparameters['weights'])
    layer_names = net.getLayerNames()
    layer_names = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    height, width = frame.shape[:2]
    frame, _,_,_,_ = infer_image(net, layer_names, height, width, frame, colors, labels, hyperparameters)
    return frame, True

def calculateAngles(keypts, poses, frame_idx):
    joints_to_draw = ['Left knee', 'Right knee', 'Left elbow', 'Right elbow']
    parts_to_draw  = ['Left lower arm','Right lower arm','Left upper arm','Right upper arm']
    joints_to_draw_idx = [13, 14, 7, 8, 9, 10, 5, 6]
    joint_angles = []
    for joint in joints_to_draw:
        angle = int(SkeletalAngles.radian_to_degree(SkeletalAngles.joint_angle(poses,frame_idx,joint)))
        joint_angles.append(angle)
    for part in parts_to_draw:
        angle = int(SkeletalAngles.radian_to_degree(SkeletalAngles.part_angle_from_vertical(poses,frame_idx,part)))
        joint_angles.append(angle)

    angles_dict = {}
    for i in range(len(joints_to_draw_idx)):
        tmp = {}
        
        positions = keypts[joints_to_draw_idx[i]].copy()
        tmp[tuple(positions)] = joint_angles[i]
        if i < 4:
            tmp['color'] = 'black'
            angles_dict[joints_to_draw[i]] = tmp
        else:
            tmp['color'] = 'red'
            angles_dict[parts_to_draw[i-4]] = tmp
        
    
    return angles_dict

def calculateSpline(keypts):
    hip_midpoint = keypts[11].copy() + keypts[12].copy()
    hip_midpoint /= 2
    shoulder_midpoint = keypts[5].copy() + keypts[6].copy()
    shoulder_midpoint /= 2
    spline_x = np.array([hip_midpoint[0], shoulder_midpoint[0]])
    spline_y = np.array([hip_midpoint[1], shoulder_midpoint[1]])
    return spline_x, spline_y


def objectRecognition(video_input_path, hyperparameters, limit, downsample, video_output_path='./object_recog.avi'):
    # some preprocessing for object recognition
    labels = open('./object_recognition/yolov3-coco/coco-labels').read().strip().split('\n')
    # Intializing colors to represent each label uniquely
    colors = np.random.randint(0, 255, size=(len(labels), 3), dtype='uint8')
    # Load the weights and configutation to form the pretrained YOLOv3 model
    net = cv2.dnn.readNetFromDarknet(hyperparameters['config'], hyperparameters['weights'])
    # Get the output layer names of the model
    layer_names = net.getLayerNames()
    layer_names = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    try:
        vid = cv2.VideoCapture(video_input_path)
        height, width = None, None
        writer = None
        frame_count = int(vid.get(cv2.CAP_PROP_FRAME_COUNT))
    except:
        raise 'Video cannot be loaded!\n\
                Please check the path provided!'

    finally:
        if limit == -1:
            final_limit = frame_count-1
        else:
            final_limit = limit
        cc = 1
        output_vals = []
        while True and cc <= final_limit:
            # if cc%downsample == 0:
            grabbed, frame = vid.read()
            print(frame.shape)
            # Checking if the complete video is read
            if not grabbed:
                break

            if width is None or height is None:
                height, width = frame.shape[:2]

            frame, boxes, confidences, classids, idxs = infer_image(net, layer_names, height, width, frame, colors, labels, hyperparameters)
            out = {}
            out['frame_index'] = cc
            out['boxes'] = boxes
            out['confidence'] = confidences
            out['classids'] = classids
            out['idxs'] = idxs
            output_vals.append(out)

            if writer is None:
                # Initialize the video writer
                fourcc = cv2.VideoWriter_fourcc(*"MJPG")
                writer = cv2.VideoWriter(video_output_path, fourcc, 30, 
                                (frame.shape[1], frame.shape[0]), True)

            writer.write(frame)
            cc += 1
        
        np.save("./object_detection_output.npy", output_vals)
        print ("[INFO] Cleaning up...")
        writer.release()
        vid.release()
**********************************************************************************************
* The github repo for this project are:
*
* Human Pose Estimation: https://github.com/facebookresearch/VideoPose3D
* Object Recognition: https://github.com/iArunava/YOLOv3-Object-Detection-with-OpenCV
**********************************************************************************************



-------------------------------------------------------
Some of the important command arguments are :
-------------------------------------------------------

  -k or --keypoints : specifies the 2D detections to use. Default: cpn_ft_h36m_dbb (CPN fine-tuned on Human 3.6M).
  -d or --dataset: specifies the dataset to use (h36m or humaneva15). Default: h36m
  -c or --checkpoint: specifies the directory where checkpoints are saved/read. Default: checkpoint
  -arc or --architecture: Represents the size of different layers in the model. Default: (3,3,3)
  --downsample: reduce the dataset frame rate by an integer factor. Default: 1 (i.e. disabled).
  --evaluate: To signal the model to perform testing (and not training)
  --render: To render videos
  --viz-camera: camera to render (integer), from 0 to 3 for Human3.6M, 0 to 2 for HumanEva. Default: 0.
  --viz-subject: subject to render, e.g. S1.
  --viz-action: action to render, e.g. Walking or Walking 1.
  --viz-video: path to the 2D video to show. If specified, the script will render a skeleton overlay on top of the video. If not specified, a black background will be rendered instead (but the 2D detections will still be shown).
  --viz-skip: skip the first N frames from the specified video.
  --viz-output: output file name (either a .mp4 or .gif file).
  -viz-limit: render only first N frames. By default, all frames are rendered.
  --viz-downsample: downsample videos by the specified factor, i.e. reduce the frame rate. E.g. if set to 2, the frame rate is reduced from 50 FPS to 25 FPS. Default: 1 (no downsampling).
  --viz-size: output resolution multiplier. Higher = larger images. Default: 5.
  --viz-export: export 3D joint coordinates (in camera space) to the specified NumPy archive.

--------------------------------------------------------------------------
Example command line to use the pretrained model and see visualizations:
--------------------------------------------------------------------------

>>> python run.py -k cpn_ft_h36m_dbb -arc 3,3,3,3,3 -c checkpoint --evaluate pretrained_h36m_cpn.bin --render --viz-subject S11 --viz-action Walking --viz-camera 0 --viz-video  data/original\ h36\ data/S11/Videos/Videos_S11/S11/Videos/Walking\ 1.54138969.mp4 --viz-output output.gif --viz-size 3 --viz-downsample 2 --viz-limit 60 --viz-export './3d-cordinates.npy'

For the above executed code, all the keypoints values will be saved in a file called 3d-cordinates.npy file.

-----------------------------------------
Code to run 2d, 3d, object extraction:
-----------------------------------------

>> python run_video_2.py --resolution=432x368 --model=cmu --video=/path/to/the/video/mp4

Note: 2d architectures in the decreasing level of accuracy are ['cmu / mobilenet_v2_large /mobilenet_thin / mobilenet_v2_small']
      Hence 'cmu' model gives higher accuracy, but is very time consuming too!
      The code to extract object and 3d keypoints (above at line 36) is run inside the run_video_2.py function

      For more details on inference in the wild: https://github.com/facebookresearch/VideoPose3D/blob/master/INFERENCE.md
